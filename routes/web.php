<?php

use Illuminate\Support\Facades\Route;
use App\Models\User as Usuario;
use App\Http\Controllers\TasksController;
use App\Http\Controllers\LogsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    $users = Usuario::where('id','!=',Auth::user()->id)->orderBy('name','ASC')->get();
    return view('dashboard')->with(['users' => $users]);
})->middleware(['auth'])->name('dashboard');

// LOGIN y registro

Route::post('/autenticacion/registro', [App\Http\Controllers\User::class, 'registro'])->name('register.post');
Route::post('/autenticacion/login', [App\Http\Controllers\User::class, 'login'])->name('login.post');

// TAREAS 
Route::post('/tareas/get/data', [TasksController::class, 'getData'])->name('tareas.getData');
Route::post('/tareas/store', [TasksController::class, 'store'])->name('tarea.store');

Route::get('/tareas/ver/{id}', [TasksController::class, 'ver'])->name('tareas.ver');

// REGISTROS
Route::post('/registros/get/data', [LogsController::class, 'getData'])->name('registros.getData');
Route::post('/registros/store', [LogsController::class, 'store'])->name('registro.store');

require __DIR__.'/auth.php';
