<?php

namespace App\Http\Controllers;

use App\Models\Tasks;
use Illuminate\Http\Request;
use App\Models\User as Usuario;
use App\Models\Logs;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function getData() {
        $query = Tasks::orderBy('id','DESC')->get();
        foreach($query as $q) {
            $q['fechaMaxima'] = date('d/m/Y', strtotime($q->fecha_maxima));
            $q['user_name'] = Usuario::where('id',$q->user_id)->pluck('name')->first();
            $q['idEncrypted'] = encrypt($q->id);
        }

        return response()->json(['estado' => 200,'data' => $query]);
    }

    public function ver($id) {
        $id = decrypt($id);
        $query = Tasks::where('id',$id)->first();
        $query['fechaMaxima'] = date('d/m/Y', strtotime($query->fecha_maxima));
        $query['user_name'] = Usuario::where('id',$query->user_id)->pluck('name')->first();
        $registros = Logs::where('task_id', $id)->get();
        foreach($registros as $r) {
            $r['fecha'] = date('d/m/Y H:i', strtotime($r->created_at));
        }
        return view('verTarea')->with(['data' => $query,'registros' => $registros]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $descripcion = $request->descripcion;
        $usuario = $request->usuario;
        $fechaMaxima = $request->fechaMaxima;

        $newTarea = new Tasks;
        $newTarea->descripcion = $descripcion;
        $newTarea->user_id = $usuario;
        $newTarea->fecha_maxima = date('Y-m-d', strtotime($fechaMaxima));
        $newTarea->save();

        return response()->json(['estado' => 200]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tasks  $tasks
     * @return \Illuminate\Http\Response
     */
    public function show(Tasks $tasks)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tasks  $tasks
     * @return \Illuminate\Http\Response
     */
    public function edit(Tasks $tasks)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tasks  $tasks
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tasks $tasks)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tasks  $tasks
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tasks $tasks)
    {
        //
    }
}
