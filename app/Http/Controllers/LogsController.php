<?php

namespace App\Http\Controllers;

use App\Models\Logs;
use Illuminate\Http\Request;

class LogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getData(Request $request) {
        $id = $request->id;
        $registros = Logs::where('task_id', $id)->get();
        foreach($registros as $r) {
            $r['fecha'] = date('d/m/Y H:i', strtotime($r->created_at));
        }

        return response()->json(['estado' => 200, 'data' => $registros]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new = new Logs;
        $new->task_id = $request->id;
        $new->comentario = $request->comentario;
        $new->save();

        $query = Tasks::where('id', $request->id)
        ->join('users','users.id','tasks.user_id')
        ->select('users.*', 'tasks.descripcion')->first();
        
        if($query) {
            \Mail::to($query->email)->send(new \App\Mail\AvisoNuevoRegistro($new, $query->descripcion, Auth::user()->name));
        }
        
        return response()->json(['estado' => 200]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Logs  $logs
     * @return \Illuminate\Http\Response
     */
    public function show(Logs $logs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Logs  $logs
     * @return \Illuminate\Http\Response
     */
    public function edit(Logs $logs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Logs  $logs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Logs $logs)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Logs  $logs
     * @return \Illuminate\Http\Response
     */
    public function destroy(Logs $logs)
    {
        //
    }
}
