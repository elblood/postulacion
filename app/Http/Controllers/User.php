<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User as Usuario;

class User extends Controller
{

    public function noPermitidos($usuario) {
        $array_no_permitidos = ["email@hack.net"];
        if (in_array($usuario, $array_no_permitidos)) return true;
        else return false;
    }

    public function login(Request $request) {
        $usuario = $request->usuario;
        $contra = $request->contra;

        $existe = Usuario::where('email', $usuario)->first();
        if($existe) {
            if(Hash::check($contra,$existe->password)) {
                $user = Usuario::find($existe->id);
                Auth::login($user);
                return response()->json(['estado' => 200]);
            } else {
                return response()->json(['estado' => 405]);
            }
        } else {
            return response()->json(['estado' => 404]);
        }

    }


    public function registro(Request $request) {
        $nombre = $request->nombre;
        $usuario = $request->usuario;
        $contra = $request->contra;

        $existe = Usuario::where('email', $usuario)->first();
        if($this->noPermitidos($usuario)) {
            return response()->json(['estado' => 405]);
        } else if(!$existe) {
            $new = new Usuario;
            $new->email = $usuario;
            $new->name = $nombre;
            $new->password = bcrypt($contra);
            $new->save();

            Auth::login($new);

            return response()->json(['estado' => 200]);
        } else {
            return response()->json(['estado' => 404]);
        }
    }


}
