<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AvisoNuevoRegistro extends Mailable
{
    use Queueable, SerializesModels;

    public $registro;
    public $descripcion;
    public $nombre;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($registro, $descripcion, $nombre)
    {
        $this->registro = $registro;
        $this->descripcion = $descripcion;
        $this->nombre = $nombre;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Se añadio un nuevo registro a una tarea")->view('mail.nuevoregistro');
    }
}
