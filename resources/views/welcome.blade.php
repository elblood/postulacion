@extends('layouts.basic')
@section('contenido')
        <h1 class="text-center font-weight-bold mt-5">Bienvenido</h1>
        <h6 class="text-center">Debes iniciar sesion para poder gestionar tareas</h6>
        
        <form class="card ingresoForm w-50 mx-auto my-3" url-to-login="{{ route('login.post') }}">
            <div class="card-body">
                <div class="my-2">
                    <input class="form-control" placeholder="Correo electronico" id="correo" type="email" />
                </div>
                <div class="my-2">
                    <input class="form-control" placeholder="Contraseña" id="contra" type="password" />
                </div>
                <button type="button" class="btn btn-primary w-100 my-3" onclick="ingresar()">Ingresar</button>
                <p class="w-100 text-center">No tienes una cuenta? <span onclick="registerMode()" class="color-blue cursor-pointer"> Registrate aqui! </span></p>
            </div>
        </form>

        <form class="card registroForm w-50 mx-auto my-3 d-none" url-to-register="{{ route('register.post') }}">
            <div class="card-body">
                <div class="my-2">
                    <input class="form-control" placeholder="Nombre" id="Rnombre" type="email" />
                </div>
                <div class="my-2">
                    <input class="form-control" placeholder="Correo electronico" id="Rcorreo" type="email" />
                </div>
                <div class="my-2">
                    <input class="form-control" placeholder="Contraseña" id="Rcontra" type="password" />
                </div>
                <div class="my-2">
                    <input class="form-control" placeholder="Confirmar Contraseña" id="Rcontra2" type="password" />
                </div>
                <button type="button" class="btn btn-primary w-100 my-3" id="registrarseBtn" onclick="registrarse()">Registrarse</button>
                <p class="w-100 text-center">ya tienes una cuenta? <span onclick="loginMode()" class="color-blue cursor-pointer"> Ingresa aqui! </span></p>
            </div>
        </form>


    {{--  Seccion de Scripts  --}}
    
    <script>
        // ALERTA SweetAlert
        

        function registerMode() {
            document.getElementsByClassName('ingresoForm')[0].classList.add('d-none');
            document.getElementsByClassName('registroForm')[0].classList.remove('d-none');
        }

        function loginMode() {
            document.getElementsByClassName('ingresoForm')[0].classList.remove('d-none');
            document.getElementsByClassName('registroForm')[0].classList.add('d-none');
        }

        function ingresar() {
            if(validarLogin()) {
                var formData = new FormData();
                formData.append('usuario', document.getElementById("correo").value);
                formData.append('contra', document.getElementById("contra").value);

                var xhttp = new XMLHttpRequest();
                xhttp.open("POST", document.getElementsByClassName("ingresoForm")[0].getAttribute("url-to-login"), true);
                xhttp.setRequestHeader('X-CSRF-Token', document.querySelector('meta[name=csrf-token]').content);
                xhttp.send(formData);
                xhttp.onload = function () {
                    console.log(this.responseText);
                    if(JSON.parse(this.responseText).estado == "200") {
                        window.location.href = "/dashboard";
                    } else if(JSON.parse(this.responseText).estado == "404") {
                        Toast.fire({
                            icon: 'warning',
                            title: 'El usuario no existe'
                        })
                        return false;
                    } else if(JSON.parse(this.responseText).estado == "405") {
                        Toast.fire({
                            icon: 'warning',
                            title: 'El usuario/Contraseña no coinciden'
                        })
                        return false;
                    }
                };
            }
        }

        function registrarse() {
            if(validarRegistro()) {
                var formData = new FormData();
                formData.append('nombre', document.getElementById("Rnombre").value);
                formData.append('usuario', document.getElementById("Rcorreo").value);
                formData.append('contra', document.getElementById("Rcontra").value);

                var xhttp = new XMLHttpRequest();
                xhttp.open("POST", document.getElementsByClassName("registroForm")[0].getAttribute("url-to-register"), true);
                xhttp.setRequestHeader('X-CSRF-Token', document.querySelector('meta[name=csrf-token]').content);
                xhttp.send(formData);
                xhttp.onload = function () {
                    console.log(this.responseText);
                    if(JSON.parse(this.responseText).estado == "200") {
                        window.location.href = "/dashboard";
                    } else if(JSON.parse(this.responseText).estado == "404") {
                        Toast.fire({
                            icon: 'warning',
                            title: 'El usuario ya existe'
                        })
                        return false;
                    } else if(JSON.parse(this.responseText).estado == "405") {
                        Toast.fire({
                            icon: 'warning',
                            title: 'El usuario no es permitido'
                        })
                        return false;
                    }
                };
            }
        }

        function validarLogin() {
            var correo = document.getElementById('correo');
            var contra = document.getElementById('contra');
            if(correo.value == "") {
                Toast.fire({
                            icon: 'warning',
                            title: 'Debe completar el campo Correo electronico'
                        })
                return false;
            } else if(contra.value == "") {
                Toast.fire({
                            icon: 'warning',
                            title: 'Debe completar el campo Contraseña'
                        })
                return false;
            }
            return true;
        }

        function validarRegistro() {
            var nombre = document.getElementById('Rnombre');
            var correo = document.getElementById('Rcorreo');
            var contra = document.getElementById('Rcontra');
            var contra2 = document.getElementById('Rcontra2');
            if(nombre.value == "") {
                Toast.fire({
                            icon: 'warning',
                            title: 'Debe completar el campo Nombre'
                        })
                return false;
            } else if(correo.value == "") {
                Toast.fire({
                            icon: 'warning',
                            title: 'Debe completar el campo Correo electronico'
                        })
                return false;
            } else if(contra.value == "") {
                Toast.fire({
                            icon: 'warning',
                            title: 'Debe completar el campo Contraseña'
                        })
                return false;
            } else if(contra2.value == "") {
                Toast.fire({
                            icon: 'warning',
                            title: 'Debe completar el campo Confirmacion de contraseña'
                        })
                return false;
            } else if(contra.value != contra2.value) {
                Toast.fire({
                            icon: 'warning',
                            title: 'Las contraseñas deben coincidir'
                        })
                return false;
            }
            return true;
        }

    </script>
@endsection
