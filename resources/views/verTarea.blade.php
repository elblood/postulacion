@extends('layouts.basic')
@section('contenido')
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="card" style="background-color:white;">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table" >
                        <thead>
                            <th>Tarea</th>
                            <th>Tiempo maximo de ejecucion</th>
                            <th>Usuario</th>
                        </thead>
                        <tbody>
                            <tr>
                            <td>{{ $data->descripcion }}</td>
                            <td>{{ $data->fechaMaxima}}</td>
                            <td>{{ $data->user_name}}</td>
                    </table>
                </div>

                <div class="d-flex mt-6 mb-3">
                    <button type="button" class="btn btn-primary m-auto ms-0" id="nuevoRegistro">Nuevo registro</button>
                    <h1 class="m-auto me-0 ">Registros asociados</h1>
                </div>
                <div class="table-responsive">
                    <table class="table body-table" url-to-data="{{route('registros.getData') }}" token-id-task="{{ $data->id }}">
                        <thead>
                            <th>Comentario</th>
                            <th>Fecha</th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</x-app-layout>

<!-- Modal -->
<div class="modal fade" id="nuevoRegistroModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h6>Nuevo registro</h6>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <textarea class="form-control mt-3" id="comentario" placeholder="Comentario del registro"></textarea>

        <button class="btn btn-primary mt-3" url-to-create="{{ route('registro.store') }}" id="nuevoRegistroRegistrar">Añadir nueva registro</button>
      </div>
    </div>
  </div>
</div>

@endsection

@section('scripts')
    <script>
        var data = [];
        var nuevoRegistro = new bootstrap.Modal(document.getElementById('nuevoRegistroModal'), {});

        getData();

        function validarNuevoRegistro() {
            var comentario = document.getElementById("comentario").value;

            if(comentario == "") {
                Toast.fire({
                            icon: 'warning',
                            title: 'Debe completar el campo Comentario de registro'
                        })
                return false;
            } 
            return true;
        }

        document.getElementById('nuevoRegistro').addEventListener("click",function () {
            var inputs = document.querySelectorAll('.form-control');
            inputs.forEach(element => {
                element.value = "";
            });
            nuevoRegistro.show();
        }, false);
        
        document.getElementById('nuevoRegistroRegistrar').addEventListener("click",function () {
            if(validarNuevoRegistro()) {
                var formData = new FormData();
                formData.append('comentario', document.getElementById("comentario").value);
                formData.append('id', document.getElementsByClassName('body-table')[0].getAttribute("token-id-task"));

                var xhttp = new XMLHttpRequest();
                xhttp.open("POST", document.getElementById('nuevoRegistroRegistrar').getAttribute("url-to-create"), true);
                xhttp.setRequestHeader('X-CSRF-Token', document.querySelector('meta[name=csrf-token]').content);
                xhttp.send(formData);
                xhttp.onload = function () {
                    console.log(this.responseText);
                    nuevoRegistro.hide();
                    getData();
                    if(JSON.parse(this.responseText).estado == "200") {
                        Toast.fire({
                            icon: 'success',
                            title: 'Nuevo registro añadido correctamente'
                        })
                    } else {
                        Toast.fire({
                            icon: 'danger',
                            title: 'Ocurrio un error'
                        })
                    }
                }
            }
        });


        function getData() {
            var formData = new FormData();
            formData.append('id', document.getElementsByClassName('body-table')[0].getAttribute("token-id-task"));

            var xhttp = new XMLHttpRequest();
            xhttp.open("POST", document.getElementsByClassName('body-table')[0].getAttribute("url-to-data"), true);
            xhttp.setRequestHeader('X-CSRF-Token', document.querySelector('meta[name=csrf-token]').content);
            xhttp.send(formData);
            xhttp.onload = function () {
                console.log(this.responseText);
                nuevoRegistro.hide();
                if(JSON.parse(this.responseText).estado == "200") {
                    data = JSON.parse(this.responseText).data;
                    completarData();
                }
            };
        }

        function completarData() {
            var dataTo = "";

            data.forEach( function(valor, indice) {
                dataTo += '<tr>';
                dataTo += '<td>'+valor.comentario+'</td>';
                dataTo += '<td>'+valor.fecha+'</td>';
                dataTo += '</tr>';

            });

            document.getElementsByClassName("body-table")[0].lastElementChild.innerHTML = dataTo;
        }
        
    </script>
@endsection