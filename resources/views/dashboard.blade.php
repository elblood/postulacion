@extends('layouts.basic')
@section('contenido')
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="card" style="background-color:white;">
            <div class="card-body">
                <button type="button" class="btn btn-primary mb-3" id="nuevaTarea">Nueva tarea</button>
                <div class="table-responsive">
                    <table class="table body-table" url-to-data="{{route('tareas.getData') }}">
                        <thead>
                            <th>Tarea</th>
                            <th>Tiempo maximo de ejecucion</th>
                            <th>Usuario</th>
                            <th>Accion</th>
                        </thead>
                        <tbody>
                            
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

<!-- Modal -->
<div class="modal fade" id="nuevaTareaModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h6>Nueva tarea</h6>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <textarea class="form-control mt-3" id="descripcion" placeholder="Descripcion de tarea"></textarea>
        <select id="usuario" class="form-control mt-3">
            <option value="" disabled selected>Asigne a un usuario</option>
            @foreach($users as $u)
                <option value="{{$u->id}}">{{$u->name}}</option>
            @endforeach
        </select>
        <input class="form-control mt-3" id="fechaMaxima" type="date" />

        <button class="btn btn-primary mt-3" url-to-create="{{ route('tarea.store') }}" id="nuevaTareaRegistrar">Añadir nueva tarea</button>
      </div>
    </div>
  </div>
</div>

@endsection

@section('scripts')
    <script>
        var data = [];
        var nuevaTarea = new bootstrap.Modal(document.getElementById('nuevaTareaModal'), {});

        getData();

        function validarNuevaTarea() {
            console.log(document.getElementById("fechaMaxima").value)
            var descripcion = document.getElementById("descripcion").value;
            var usuario = document.getElementById("usuario").value;
            var fechaMaxima = document.getElementById("fechaMaxima").value;

            if(descripcion == "") {
                Toast.fire({
                            icon: 'warning',
                            title: 'Debe completar el campo Descripcion de tarea'
                        })
                return false;
            } else if(usuario == null || usuario == "") {
                Toast.fire({
                            icon: 'warning',
                            title: 'Debe asignar la tarea a un usuario'
                        })
                return false;
            } else if(fechaMaxima == null || fechaMaxima == "") {
                Toast.fire({
                            icon: 'warning',
                            title: 'Debe completar la fecha maxima de ejecucion'
                        })
                return false;
            }
            return true;
        }

        document.getElementById('nuevaTarea').addEventListener("click",function () {
            var inputs = document.querySelectorAll('.form-control');
            inputs.forEach(element => {
                element.value = "";
            });
            nuevaTarea.show();
        }, false);

        function getData() {
            var xhttp = new XMLHttpRequest();
            xhttp.open("POST", document.getElementsByClassName('table')[0].getAttribute("url-to-data"), true);
            xhttp.setRequestHeader('X-CSRF-Token', document.querySelector('meta[name=csrf-token]').content);
            xhttp.send();
            xhttp.onload = function () {
                console.log(this.responseText);
                nuevaTarea.hide();
                if(JSON.parse(this.responseText).estado == "200") {
                    data = JSON.parse(this.responseText).data;
                    completarData();
                }
            };
        }

        function completarData() {
            var dataTo = "";

            let date = new Date()

            let day = date.getDate()
            if(day.length == 1) day = "0"+day;
            console.log(day)
            let month = date.getMonth() + 1
            if(month.length == 1) month = "0"+month;
            let year = date.getFullYear()

            data.forEach( function(valor, indice) {
                var atrasado = "";
                if(parseInt(valor.fecha_maxima.replace('-','').replace('-','')) < parseInt(year+""+month+""+day)) {
                    atrasado = "atrasado";
                }
                dataTo += '<tr>';
                dataTo += '<td class="text-truncate '+atrasado+'">'+valor.descripcion+'</td>';
                dataTo += '<td class="'+atrasado+'">'+valor.fechaMaxima+'</td>';
                dataTo += '<td class="'+atrasado+'">'+valor.user_name+'</td>';
                if(document.querySelector('meta[name=token-id]').content == valor.user_id) dataTo += '<td class="'+atrasado+'"><a href="/tareas/ver/'+valor.idEncrypted+'">Ver</a></td>';
                else dataTo += '<td></td>';

                dataTo += '</tr>';
            });

            document.getElementsByClassName("body-table")[0].lastElementChild.innerHTML = dataTo;
        }

        document.getElementById('nuevaTareaRegistrar').addEventListener("click",function () {
            if(validarNuevaTarea()) {
                var formData = new FormData();
                formData.append('descripcion', document.getElementById("descripcion").value);
                formData.append('usuario', document.getElementById("usuario").value);
                formData.append('fechaMaxima', document.getElementById("fechaMaxima").value);

                var xhttp = new XMLHttpRequest();
                xhttp.open("POST", document.getElementById('nuevaTareaRegistrar').getAttribute("url-to-create"), true);
                xhttp.setRequestHeader('X-CSRF-Token', document.querySelector('meta[name=csrf-token]').content);
                xhttp.send(formData);
                xhttp.onload = function () {
                    console.log(this.responseText);
                    nuevaTarea.hide();
                    getData();
                    if(JSON.parse(this.responseText).estado == "200") {
                        Toast.fire({
                            icon: 'success',
                            title: 'Nueva tarea añadida correctamente'
                        })
                    } else {
                        Toast.fire({
                            icon: 'danger',
                            title: 'Ocurrio un error'
                        })
                    }
                }
            }
        });
    </script>
@endsection