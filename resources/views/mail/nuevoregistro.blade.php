<p>Se informa que se ha añadido un nuevo registro a una tarea que creaste.</p>
<br/>
<p>{{ $descripcion }}</p>
<br/>
<br/>
<p>El usuario {{ $nombre }}, ha agregado el registro:</p>
<br/>
<p>{{ $registro->comentario }}

